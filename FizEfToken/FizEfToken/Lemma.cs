﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace FizEfToken
{
    class Lemma
    {
        static public void doLemma(String folder)
        {
            string lineInFile = "";
            StreamReader sr = new StreamReader(folder + "\\2.txt");
            StreamWriter sw = new StreamWriter(folder + "\\2_2.txt");
            while ((lineInFile = sr.ReadLine()) != null)
            {
                String [] massOfWords = lineInFile.Split('\t');
                if (massOfWords[2] == "<unknown>")
                {
                    sw.WriteLine(massOfWords[0]);
                }
            }
            sr.Close();
            sw.Close();

            // Создать новый процесс
            Process proc = new Process();
            folder = folder + "\\cstlemma.exe";
            String Args = "-L -p+ -q- -t- -U- -H2 -m0 -l -B '$w' -b '$w' -u- -c $w/$b[[$b0]?$B]$s -f flexrules0 -i 2_2.txt -o 2_output_2.txt";
            proc.StartInfo = new ProcessStartInfo(folder, Args);
            proc.StartInfo.WorkingDirectory = "C:\\Users\\qwerty\\workspace\\FizEf\\Lemmatiser\\1";
            proc.Start();
            proc.WaitForExit();

        }
    }
}
