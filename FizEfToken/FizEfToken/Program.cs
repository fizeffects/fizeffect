﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace FizEfToken
{
    class Program
    {
        static void Main(string[] args)
        {

            string pathForTaggerCstLemma = "C:\\Users\\qwerty\\workspace\\FizEf\\Lemmatiser\\1";
            TreeTagger.startTreeTagger(pathForTaggerCstLemma);
            Lemma.doLemma(pathForTaggerCstLemma);

            StreamReader sr = new StreamReader(pathForTaggerCstLemma + "\\2_output_2.txt");
            string lineOfCstLemmaWords;
            List<string> listUnknownWords = new List<string>();
            while ((lineOfCstLemmaWords = sr.ReadLine()) != null)
            {
                listUnknownWords.Add(lineOfCstLemmaWords);
            }
            sr.Close();

            StreamReader sr1 = new StreamReader(pathForTaggerCstLemma + "\\2.txt");
            StreamWriter sw = new StreamWriter(pathForTaggerCstLemma + "\\output_all.txt");
            string line; int i = 0;
            while ((line = sr1.ReadLine()) != null)
            {
                String[] massOfWords = line.Split('\t');
                if (massOfWords[2] == "<unknown>")
                {
                    sw.WriteLine(massOfWords[0] + "\t" + massOfWords[1] + "\t" + listUnknownWords[i].Split('/')[1].Replace("'", ""));
                    i++;
                }
                else sw.WriteLine(line);
            }
            sr1.Close();
            sw.Close();


    
        }
    }
}
