﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FizEfToken
{
    class Tokenize
    {
        //characters which have to be cut off at the beginning of a word
        public String PChar = "\\[¿¡{(\\`\"‚„†‡‹‘’“”•–—›'.'";

        // characters which have to be cut off at the end of a word
        public String FChar = "]}\'" + "`\")" + ",;:!?%‚„…†‡‰‹‘’“”•–—›";

        //character sequences which have to be cut off at the beginning of a word
        public String PClitic = "";

        //character sequences which have to be cut off at the end of a word
        public String FClitic = "";

        public static String S1 = "";
        public static String suffix = "";

        public static String tokenRus(String str)
        {
            String tokenStr = Tokenize.tokenize(str);
            return tokenStr;

        }

        private bool isTrue(String s, String str)
        {
            Match isMatch = Regex.Match(str, s, RegexOptions.IgnoreCase);
            return isMatch.Success;
        }

        public static String tokenize(String str)
        {
            S1 = "";
            suffix = "";
            Tokenize token = new Tokenize();
            token.convert(str);
            return S1;
        }

        private void convert(String str)
        {
            Match isMatch = Regex.Match(str, "^\uFEFF", RegexOptions.IgnoreCase);
            str = isMatch.Value; //????
            str.Replace("\n", " ");
            str = str.Replace("\n", " ");
            str = str.Replace('\t', ' ');


            isMatch = Regex.Match(str, "(<[^<> ]*) ([^<>]*>)");
            str = Regex.Replace(str, "(<[^<> ]*) ([^<>]*>)", "$1\u0377$2");
           // str = isMatch.Value;
            str = Regex.Replace(str, " ", "\u0376"); //???????
            str = Regex.Replace(str, "\u0377\u0376", " \u0377");

            isMatch = Regex.Match(str, "(<[^<>]*>)");
            str = isMatch.Value.Replace("(<[^<>]*>)", "$1\u0377"); //???

            isMatch = Regex.Match(str, "^\u0377", RegexOptions.IgnoreCase);
            str = isMatch.Value.Replace("^\u0377", ""); //????

            isMatch = Regex.Match(str, "\u0377$", RegexOptions.IgnoreCase);
            str = isMatch.Value.Replace("\u0377$", ""); //????

        //    isMatch = Regex.Match(str, "\u0377\u0377\u0377*", RegexOptions.IgnoreCase);
            str = Regex.Replace(str, "\u0377\u0377\u0377*", "\u0377"); //????
          

            String[] S = Regex.Split(str, "\u0376");

            for (int i = 0; i < S.Length; i++)
            {
                String local = S[i];
                if (isTrue("^<.*>$", local))
                {
                    S1 = S1 + local + "\n";
                }
                else
                {
                    local = " " + local + " ";

                    
                   // isMatch = Regex.Match(local, "(\\.\\.\\.)", RegexOptions.IgnoreCase);
                    local = Regex.Replace(local, "(\\.\\.\\.)", " ... ");
                 //   isMatch = Regex.Match(local, "([;\\!\\?])([^ ])", RegexOptions.IgnoreCase);
                    local = Regex.Replace(local, "([;\\!\\?])([^ ])", "$1 $2");
                 //   isMatch = Regex.Match(local, "([.,:])([^ 0-9.])", RegexOptions.IgnoreCase);
                    local = Regex.Replace(local, "([.,:])([^ 0-9.])", "$1 $2");

                    String[] F = Regex.Split(local, " ");
                    for (int j = 0; j < F.Length; j++)
                    {
                        if (F[j].Length > 0)
                        {
                            firstStage(F[j]);
                        }
                    }
                }
            }
        }

        private void firstStage(String str)
        {
    
            Match isMatch;

            int finished = 1;
            int tt = 0;
            suffix = "";
            do
            {
                tt++;
                if (tt == 10000)
                {
                    tt++;
                }
                finished = 1; //for loop cycle

                //	    cut off preceding punctuation
                //	  if (s/^([$PChar])(.)/$2/) {
                //	    $S1 = $S1 . $1,"\n";
                //	    $finished = 0;
                //	  }


                isMatch = Regex.Match(str, "^([" + PChar + "])(.+)");
                if (isMatch.Success)
                {
                    S1 = S1 + /*matcher.group(1) + */"\n";
                    /*str = matcher.replaceAll("$2");*/
                    finished = 0;
                }

                //cut off trailing punctuation
                //	  if (s/(.)([$FChar])$/$1/) {
                //	    $suffix = "$2\n$suffix";
                //	    $finished = 0;
                //	  }

                isMatch = Regex.Match(str, "(.+)([" + FChar + "])$");
                if (isMatch.Success)
                {
                    suffix = /*matcher.group(2) + */"\n" + suffix;
                //    str = matcher.replaceAll("$1");
                    string rez = isMatch.Value;
                    str.Replace(rez, "$1");
                    finished = 0;
                }

                //cut off trailing periods if punctuation precedes
                //if (s/([$FChar])\.$//) {
                isMatch = Regex.Match(str, "([" + FChar + "])\\.$");
                if (isMatch.Success)
                {
                    String grp = /*matcher.group(1)*/"";
                    S1 = S1 + grp + "\n";
                    //str = matcher.replaceAll("");
                    string rez = isMatch.Value;
                    str.Replace(rez, "");

                    suffix = ".\n" + suffix;
                    if (str == "")
                    {
                        str = grp;
                    }
                    else
                    {
                        suffix = grp + "\n" + suffix;
                    }

                    finished = 0;
                }
            } while (!(finished == 1));


            //handle explicitly listed tokensopen
            //$Token alwayas undef
            //	if (defined($Token{$_})) {
            //	  S1 = S1 +  "$_\n$suffix";
            //	  next;
            //	}

            //abbreviations of the form A. or U.S.A.
            isMatch = Regex.Match(str, "^([A-Za-z-]\\.)+$");
            if (isMatch.Success)
            {
                S1 = S1 + str + "\n" + suffix;
                return;
            }

            //disambiguate periods
            if (isTrue("^(..*)\\.$", str) && str != "...")
            {
                //if (/^(..*)\.$/ && $_ ne "..." && )
                //always true = !($opt_g && /^[0-9]+\.$/)) {
                isMatch = Regex.Match(str, "^(..*)\\.$");
                if (isMatch.Success)
                {
                    /*str = matcher.group(1);*/
                    suffix = ".\n" + suffix;
                }
                /*if (defined($Token{$_})) {
                $S1 = $S1 . "$_\n$suffix";
                next;
                } */
            }

            //cut off clitics
            while (isTrue("^(--)(.)", str))
            {
                isMatch = Regex.Match(str, "^(..*)\\.$");
                if (isMatch.Success)
                {
                    S1 = S1 + /*matcher.group(1) +*/ "\n";
                    //str = matcher.replaceAll("$2");

                    string rez = isMatch.Value;
                    str.Replace(rez, "$2");
                }
            }

            if (PClitic != "")
            {
                while (isTrue("^(" + PClitic + ")(.)", str))
                {
                    
                    isMatch = Regex.Match(str, "^(\"+PClitic+\")(.)");
                    if (isMatch.Success)
                    {
                        S1 = S1 + /*matcher.group(1) + */"\n";
                       // str = matcher.replaceAll("$2");

                        string rez = isMatch.Value;
                        str.Replace(rez, "$2");
                    }
                }
            }

            while (isTrue("(.)(--)$", str))
            {
                
                isMatch = Regex.Match(str, "(.)(--)$");
                if (isMatch.Success)
                {
                    suffix = /*matcher.group(2) + */"\n" + suffix;
                    //str = matcher.replaceAll("$2");

                    string rez = isMatch.Value;
                    str.Replace(rez, "$2");
                }
            }

            if (FClitic != "")
            {
                while (isTrue("(.)(" + FClitic + ")$", str))
                {
                    isMatch = Regex.Match(str, "(.)(" + FClitic + ")$");
                    if (isMatch.Success)
                    {
                        suffix = /*isMatch.Group(2) + */"\n" + suffix;
                        //str = matcher.replaceAll("$2");

                        string rez = isMatch.Value;
                        str.Replace(rez, "$2");
                    }
                }
            }

            S1 = S1 + str + "\n" + suffix;
        }


    }
}
